package KNN;
//import java.io.FileNotFoundException;
import java.io.*;
//import java.io.PrintWriter;
//import java.io.IOException;
import java.awt.*;
import java.awt.event.*;
import java.util.Random;
import java.util.ArrayList;
import java.lang.*;
import java.math.*;
import java.util.concurrent.TimeUnit;

public class knn extends Frame{
    public static ArrayList arrayx = new ArrayList();
    public static ArrayList arrayy = new ArrayList();
    public static int arrayGroup[];
    public static int arrayPointer = 0;
    public static int printTimedelay = 0;
    public static double mini = 1.0000000000000002E-20;
    public static int setWindowsSize = 1050;
    public static int setWindowsBorder = 50;
    public static int setPointRange = 950;
    public static Color groupColorlist[] = {Color.BLUE,Color.CYAN,Color.DARK_GRAY,Color.GREEN,Color.MAGENTA,Color.ORANGE,Color.PINK,Color.RED,Color.YELLOW};
    private static Graphics g;
    public knn(){//just the windows , not necessary
        setTitle("let's rock");
        setSize(setWindowsSize,setWindowsSize);
        setVisible(true);
        setBackground(Color.white);
        //allow to close
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e){
                System.exit(0);
            }
        });
        g = getGraphics();
    }
    public static void printerPoint(Color pointColor,int pointx,int pointy,int tag){
        g.setColor(pointColor);
        if(tag==1)g.fillOval(pointx+setWindowsBorder ,pointy+setWindowsBorder ,10,10);
        else if (tag==2)g.fillRect(pointx+setWindowsBorder ,pointy+setWindowsBorder ,15,15);
    }
    public static void generateData(int number)  {
       try {
           PrintWriter writer = new PrintWriter("test.txt");
           for (int i=1;i<=number;i++) {
               Random rdm = new Random();
               writer.print( rdm.nextInt(setPointRange)  + ",");
               writer.println(rdm.nextInt(setPointRange)  );
           }
            writer.close();
       } catch (IOException e) {
        // do something
       }
   }
    public static void dataReader(){
        String line = null;
        try{
            FileReader fr = new FileReader("test.txt");
            BufferedReader br = new BufferedReader(fr);
            while((line = br.readLine())!= null){
                //System.out.println(line);
                String[] tmp = line.split(",");
                //System.out.print(tmp[0]);
                //System.out.print(tmp[1]);
                arrayx.add(tmp[0]);
                arrayy.add(tmp[1]);
                arrayPointer++;
            }

        }catch (IOException e) {
            // do something
        }
        //int i = 0;
/**
        for(i=0;i<arrayx.size();i++){
            System.out.print(arrayx.get(i));
            System.out.print(arrayy.get(i));
        }
        System.out.println();
**/
    }
    public static double getDistance(double point[],double group[]){
        double x = Math.pow(point[0]-group[0],2);
        double y = Math.pow(point[1]-group[1],2);
        return Math.sqrt(x+y);
    }
    public static void printTest(double a[]){
        for (double i : a)System.out.print(i+"\t");
    }
    public static double nearestNeighbors(int groupNumber) {
        if(groupNumber<2) ;
        else{
            double groupPosition[][] = new double[groupNumber][2];
            Color groupColor[]=new Color[groupNumber];
            Random rdm = new Random();
            int i,j;//i for item , j for group

            for(j=0;j<groupNumber;j++){
                groupPosition[j][0]=rdm.nextInt(setPointRange);
                groupPosition[j][1]=rdm.nextInt(setPointRange);
                groupColor[j] = groupColorlist[j%groupColorlist.length];
            }

            //System.out.println("next stage");
            double movement = 0;
            int times = 0;
            //----------------------------------------------------------------------------------------------------------
            do{
                movement = 0;
                double distance = 999999999;
                for(i=0;i<arrayy.size();i++){
                    distance = 999999999;
                    for(j=0;j<groupNumber;j++){
                        double point[]={Integer.parseInt((String)arrayx.get(i)), Integer.parseInt((String)arrayy.get(i))};
                        double group[]={groupPosition[j][0],groupPosition[j][1]};
                        if(getDistance(point,group)<distance){
                            distance = getDistance(point,group);
                            arrayGroup[i]=j;
                        }
                    }
                }

                //-----------------------printer
                try {
                    TimeUnit.SECONDS.sleep(printTimedelay);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                for (i = 0; i < arrayy.size(); i++) printerPoint(groupColor[arrayGroup[i]], Integer.parseInt((String) arrayx.get(i)), Integer.parseInt((String) arrayy.get(i)), 1);
                for (j = 0; j < groupNumber; j++) printerPoint(groupColor[j], (int) groupPosition[j][0], (int) groupPosition[j][1], 2);
                //-----------------------printer

                for(j=0;j<groupNumber;j++){
                    double sumx=0,sumy=0,count=0;
                    for(i=0;i<arrayy.size();i++){
                        if(arrayGroup[i]==j){
                            sumx+=Integer.parseInt((String)arrayx.get(i));
                            sumy+=Integer.parseInt((String)arrayy.get(i));
                            count+=1;

                        }
                    }
                    if(count!=0) {
                        double ngroupPosition[] = {sumx / count, sumy / count};
                        double groupNowPosition[] = {groupPosition[j][0],groupPosition[j][1]};
                        movement+=getDistance(ngroupPosition,groupNowPosition);
                        groupPosition[j][0]=ngroupPosition[0];
                        groupPosition[j][1]=ngroupPosition[1];
                    }
                }

                times+=1;

                //-----------------------printer
                try {
                    TimeUnit.SECONDS.sleep(printTimedelay);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                for (i = 0; i < arrayy.size(); i++) printerPoint(groupColor[arrayGroup[i]], Integer.parseInt((String) arrayx.get(i)), Integer.parseInt((String) arrayy.get(i)), 1);
                for (j = 0; j < groupNumber; j++) printerPoint(groupColor[j], (int) groupPosition[j][0], (int) groupPosition[j][1], 2);
                //-----------------------printer
            }while(movement > mini);
            //System.out.println(times);
            int distanceSum = 0;
            for(i = 0; i<arrayy.size(); i++){
                double target[]={Integer.parseInt((String)arrayx.get(arrayGroup[i])), Integer.parseInt((String)arrayy.get(arrayGroup[i]))};
                distanceSum+=getDistance(groupPosition[arrayGroup[i]],target);
            }



            //-----------------------printer
            try {
                TimeUnit.SECONDS.sleep(printTimedelay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            for (i = 0; i < arrayy.size(); i++) printerPoint(groupColor[arrayGroup[i]], Integer.parseInt((String) arrayx.get(i)), Integer.parseInt((String) arrayy.get(i)), 1);
            for (j = 0; j < groupNumber; j++) printerPoint(groupColor[j], (int) groupPosition[j][0], (int) groupPosition[j][1], 2);
            //System.out.println("end");--------------------------------------------------------------------------------


            return distanceSum;
        }
        return 0;
    }
    public static void groupTestKNN(int groupNumber){
        knn myfrm = new knn();
        myfrm.show();
        int i = 0;
        for(i=0;i<arrayy.size();i++){
            int x = Integer.parseInt((String)arrayx.get(i));
            int y = Integer.parseInt((String)arrayy.get(i));
            printerPoint(Color.black,x,y,1);
        }
        myfrm.show();
        double distanceSum = nearestNeighbors(groupNumber);
        System.out.print("distance:");
        System.out.println(distanceSum);
        System.out.println("end");
    }
    public static void main(String[] args){



        generateData(1000);
        dataReader();
        arrayGroup = new int[arrayy.size()];
        int groupCount;
        for(groupCount=3;groupCount<=10;groupCount++)
            groupTestKNN(groupCount);
        /**
        knn myfrm = new knn();
        myfrm.show();
        int i = 0;
        for(i=0;i<arrayy.size();i++){
            int x = Integer.parseInt((String)arrayx.get(i));
            int y = Integer.parseInt((String)arrayy.get(i));
            printerPoint(Color.black,x,y,1);
        }
        myfrm.show();
        nearestNeighbors(5);
        System.out.println("end");**/
    }
}
